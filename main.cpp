//
//  main.cpp
//  OOP_lab
//
//  Created by Софія Голець on 14.09.2022.
//

#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

const int n = 10000;
int numbers[10] = {0,0,0,0,0,0,0,0,0,0};

vector<long> method_1(int a=4759, int c=37, int m=21411){
    long x = time(0);
    vector <long> v = {};
    for (int i=0; i < n; i++) {
        x = (a * x + c) % m;
        v.push_back(x);
        float u = (float)x / m;
        numbers[(int) (u * 10)]++;
    }
    return v;
}

vector<long> method_2(int a=16654, int c=17, int d=2379, int m=21411){
    long x = 1;
    vector <long> v = {};
    for (int i=0; i < n; i++) {
        x = (d * x * x + a * x + c) % m;
        v.push_back(x);
        float u = (float)x / m;
        numbers[(int) (u * 10)]++ ;
    }
    return v;
}

void method_3 (int m=14931) {
    long x0 = time(0);
    long x1 = time(0)+1234567;
    for (int i =  0; i < n; i++) {
        x1 = (x1 + x0) % m;
        float u = (float) x1 / m;
        numbers[(int) (u * 10)]++;
    }
}

int mod_inversion (long x0, int p) {
    for (int x1 = 0; x1 < p; x1++) {
        if ((x0 * x1) % p == 1) {
            return x1;
        }
    }
    return 0;
}


void method_4 (int a=21557, int c =822, int p=41011){
    long x = time(0) % p;
    for (int i=0; i < n; i++) {
        x = (a * mod_inversion(x, p) + c) % p;
        float u = (float)x / p;
        numbers[(int) (u * 10)]++ ;
    }
}
void clear (int *array) {
    for (int i = 0; i < 10; i++) {
        *(array+i) = 0;
    }
}

void method_5(int m=21411){
    vector<long> vx = method_1();
    vector<long> vy = method_2();
    clear(numbers);
    for (int i=0; i < n; i++) {
        long z = (vx[i] - vy[i] + m ) % m;
        float u = (float)z / m;
        numbers[(int) (u * 10)]++;
    }
}

void check_sum(int set[10]) {
    float sum = 0;
    for (int i=0; i<10; i++) {
        sum += (float) set[i]/n;
    }
    cout<<"\nThe sum is equal "<<sum<<endl;
}

void evenlyDistributed  () {
    cout << '\n' << "  Range    " << "Frequency" << endl;
    cout << "[0.0;0.1) : " << (float) numbers[0] / n  << endl;
    for (int i=1; i<9; i++) {
        float j = (float) i / 10;
        cout << "[" << j <<";"<< j + 0.1 <<") : "<< (float) numbers[i] / n << endl;
    }
    cout << "[0.9;1.0] : " << (float) numbers[9] / n << endl;
    check_sum(numbers);
}



void arrayfilling6_8(float x, int set[10]) {
    for (int i = 0; i < 10; i++) {
        if (x >= -3 + (i * 0.6) && x <= -3 + ((i+1) * 0.6)){
            set[i]++;
        }
    }
}

void normallyDistributed  (int set[10]) {
    cout << '\n' << "  Range    " << "Frequency" << endl;
    cout << "[-3.0;-2.4) : " << (float) set[0] / n << endl;
    float i = -2.4;
    int j = 1;
    while (i<2.4f){
        cout << "["<<i<<";"<<i+0.6f<<") : "<<(float) set[j] / n<<endl;
        i += 0.6f;
        j += 1;
    }
    cout << "[2.4;3.0] : " << (float) set[9] / n << endl;
    check_sum(set);
}

  
void method_6(int m=21411){
    vector<long> vx = method_1();
    clear(numbers);
    float sum;
    for (int i=0; i < n; i++) {
        sum = 0;
        for (int j=0; j < 12; j++) {
            float u = (float) vx[i+j] / m;
            sum += u;
        }
        sum -= 6;
        arrayfilling6_8(sum,numbers);
    }
    
}


void method_7(int m=21411) {
    int set[10] = {0,0,0,0,0,0,0,0,0,0};
    float U1, U2, V1, V2, S, X1, X2;
    vector<long> vx = method_1();
    clear(numbers);
    for (int i = 1; i < n; i++) {
           U1 = (float) vx[i] / m;
           U2 = (float) vx[i+1] / m;
           V1 = 2 * U1 - 1;
           V2 = 2 * U2 - 1;
           S = V1 * V1 + V2 * V2;
           if (S < 1) {
               X1 = V1 * sqrt((-2 * log(S)) / S);
               X2 = V2 * sqrt((-2 * log(S)) / S);
               arrayfilling6_8(X1, numbers);
               arrayfilling6_8(X2, set);
               
           }
       }
    normallyDistributed(numbers);
    normallyDistributed(set);
   }

void method_8(int m = 21411) {
    vector<long> vx = method_1();
    vector<long> vy = method_2();
    clear(numbers);
    for (int i = 0; i < n; i++) {
        float U = (float) vx[i]/m;
        float V = (float) vy[i]/m;
        if (U != 0) {
            float X = sqrt(8 / M_E) * (V - 0.5) / U;
            if (X * X <= -4 * log(U)) {
                arrayfilling6_8(X, numbers);
            }

        }
    }
}

void distribution9_10 () {
    cout << '\n' << "  Range         " << "Frequency" << endl;
    cout << "[0  ; 10)" << "      " << (float) numbers[0] / n << endl;
    for (int i = 1; i < 9; i++) {
        cout << "[" << i * 10 << " ; " << i * 10 + 10 << ")" << "      " << (float) numbers[i] / n << endl;
    }
    cout << "[90 ;100]" << "      " << (float) numbers[9] / n << '\n' << endl;
    check_sum(numbers);
}

void method_9 (int m = 21411, int  mu = 37) {
    vector<long> vx = method_2();
    clear(numbers);
    float  U, x;
    for (int i = 0; i < n; i++) {
        U = (float) vx[i] / m;
        x = -mu * log(U);
        if (x < 100) {
            numbers[(int) x / 10]++;
        }
    }
}

void method_10(int m=21411, int a = 11) {
    vector<long> vx = method_1();
    vector<long> vy = method_2();
    clear(numbers);
    float Y, X, U, V;
    for (int i = 0; i < n; i++) {
        U = (double) vx[i] / m;
        Y = tan(U * M_PI);
        X = sqrt(2 * a - 1) * Y + a - 1;
        if (X > 0) {
            V = (float) vy[i] / m;
            if (V <= ((1 + Y * Y) * exp((a - 1) * log(X / (a - 1)) - sqrt(2 * a - 1) * Y))) {
                    numbers[(int) X % 10]++;
            }
        }
    }
}


void methodChoice() {
    cout << "Enter generator type: " << endl;
    cout << "\t1 - Linear Congruential Generator" << endl;
    cout << "\t2 - Quadratic Congruential Generator" << endl;
    cout << "\t3 - Fibonacci Numbers Generator" << endl;
    cout << "\t4 - Inverse Congruent SequenceGenerator" << endl;
    cout << "\t5 - Union Method Generator" << endl;
    cout << "\t6 - Sigma Method Generator" << endl;
    cout << "\t7 - Polar Coordinate Method Generator" << endl;
    cout << "\t8 - Relation Method Generator" << endl;
    cout << "\t9 - Logarithm Method Generator" << endl;
    cout << "\t10 - Arens Method Generator\n" << endl;
}


int main() {
    methodChoice();

    int method;
    cin >> method;

    switch (method) {
        case 1: method_1(); evenlyDistributed(); break;
        case 2: method_2(); evenlyDistributed(); break;
        case 3: method_3(); evenlyDistributed(); break;
        case 4: method_4(); evenlyDistributed(); break;
        case 5: method_5(); evenlyDistributed(); break;
        case 6: method_6(); normallyDistributed(numbers); break;
        case 7: method_7(); break;
        case 8: method_8(); normallyDistributed(numbers); break;
        case 9: method_9(); distribution9_10(); break;
        case 10:method_10(); distribution9_10 ();break;
        default:
            cout << "Wrong input! Only 1-10 requires\n";
    }

    return 0;
}
